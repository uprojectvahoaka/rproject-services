############################        INSTALLATION             #####################################

            **************       Durée de développement 2 jours       **************

Projet Backend 
    - Le projet ici présent est le code source du projet Spring Boot. 
    - Le packet utiliser WAR , d'ou utilisation d'un serveur WEB TOMCAT 9 (De preference)
    - Environnement :  Java 8  - Maven 3.6.1 - Tomcat 9 


Base de données sur la pièce jointe 
    - Creer la base à utiliser : r-project  
    - La creation des Entités / table se fait par Spring 
    - Lancer les scripts pour les données de Voitures et Utilsiateur  dans SQL pour le cas actuel 
        (la dinamisation de ces données se fait plus tard )
    -L'authentification , l'ajout et la liste  des commmentaires sont dynamiques ici 


Running Project  (Developppment Projet)
    - Telecharger le projet via GITHUB 
    - Ouvrir avec Eclipse pour JEE : Import existing Maven Project  | Installation automatic des dépendances

    - Demarrer le projet via Eclipse , sur tomcat qui dans un premier temps aura un bug a cause d'une conflit de JAR persitence (a configurer plus tard)
    - Pour le debug ici : suprimer un des JAR en conflit 

    Exemple de lien du LIB à supperimer ici hibernate jpa persist
    Web apps Created by eclipse configuration |  Système windows 10
	< Workspace selectionner sur Eclipse >\.metadata\.plugins\org.eclipse.wst.server.core\tmp0\wtpwebapps\rproject-services\WEB-INF\lib


	- Re-run project on Server (port:8080 pour la cohérence avec Front)  
	
	 Enjoy ....    :-) 


