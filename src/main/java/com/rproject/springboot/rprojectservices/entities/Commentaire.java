package com.rproject.springboot.rprojectservices.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "commentaire")
public class Commentaire implements Serializable {

	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String message;
	private Date createDate;
	private String commentaire;
	
	@ManyToOne
	@JoinColumn(name="utilisateur_id")
	private Utilisateur utilisateur;

	@ManyToOne
	@JoinColumn(name="voiture_id")
	private Voiture voiture;
	
	public Commentaire() {
		super();
		// TODO Auto-generated constructor stub
	}
	

	public Commentaire(String message, Date createDate) {
		super();
		this.message = message;
		this.createDate = createDate;
	}

	public Commentaire(String message, Utilisateur utilisateur, Voiture voiture) {
		super();
		this.message = message;
		this.utilisateur = utilisateur;
		this.voiture = voiture;
	}


	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}


	public Utilisateur getUtilisateur() {
		return utilisateur;
	}


	public void setUtilisateur(Utilisateur utilisateur) {
		this.utilisateur = utilisateur;
	}


	public Voiture getVoiture() {
		return voiture;
	}


	public void setVoiture(Voiture voiture) {
		this.voiture = voiture;
	}


	
	
	
	
	
}
