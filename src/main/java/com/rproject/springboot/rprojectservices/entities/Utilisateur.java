package com.rproject.springboot.rprojectservices.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "utilisateur")
public class Utilisateur implements Serializable {

	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String nom;
	private String password;
	private String connected;
	
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name="utilisateur_id", referencedColumnName="id")
    private List<Commentaire> commentaires = new ArrayList<>();
	
	
	public Utilisateur() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Utilisateur(String nom, String password, String connected) {
		super();
		this.nom = nom;
		this.password = password;
		this.connected = connected;
	}

	public Utilisateur(Long id) {
		super();
		this.id = id;
	
	}

	public String getNom() {
		return nom;
	}


	public void setNom(String nom) {
		this.nom = nom;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public String getConnected() {
		return connected;
	}


	public void setConnected(String connected) {
		this.connected = connected;
	}
	
	
	
}
