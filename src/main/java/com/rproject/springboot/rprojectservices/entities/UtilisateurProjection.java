package com.rproject.springboot.rprojectservices.entities;

import org.springframework.data.rest.core.config.Projection;

@Projection(name="P1", types=Utilisateur.class)
public interface UtilisateurProjection {
	
	public String getNom();
	public Long getId();

}
