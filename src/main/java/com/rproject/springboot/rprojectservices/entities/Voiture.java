package com.rproject.springboot.rprojectservices.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "voiture")
public class Voiture implements Serializable {

	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String marque;
	private String modele;
	private String type;
	private String carburant;
	private int annee;
	private String cylendre;
	private String image;
	
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name="voiture_id", referencedColumnName="id")
    private List<Commentaire> commentaires = new ArrayList<>();
	
	
	public Voiture() {
		super();
		// TODO Auto-generated constructor stub
	}

	
	public Voiture(Long id) {
		super();
		this.id = id;
	}


	public Voiture(String marque, String modele, String type, String carburant, int annee, String cylendre,
			String image) {
		super();
		this.marque = marque;
		this.modele = modele;
		this.type = type;
		this.carburant = carburant;
		this.annee = annee;
		this.cylendre = cylendre;
		this.image = image;
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getMarque() {
		return marque;
	}


	public void setMarque(String marque) {
		this.marque = marque;
	}


	public String getModele() {
		return modele;
	}


	public void setModele(String modele) {
		this.modele = modele;
	}


	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}


	public String getCarburant() {
		return carburant;
	}


	public void setCarburant(String carburant) {
		this.carburant = carburant;
	}


	public int getAnnee() {
		return annee;
	}


	public void setAnnee(int annee) {
		this.annee = annee;
	}


	public String getCylendre() {
		return cylendre;
	}


	public void setCylendre(String cylendre) {
		this.cylendre = cylendre;
	}


	public String getImage() {
		return image;
	}


	public void setImage(String image) {
		this.image = image;
	}
	
	
	
}
