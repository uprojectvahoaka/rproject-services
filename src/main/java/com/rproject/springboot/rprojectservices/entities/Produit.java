package com.rproject.springboot.rprojectservices.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Data
@Table(name = "produit")
public class Produit implements Serializable {

	//  @NoArgsConstructor @AllArgsConstructor @ToString
	// Property
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String designation;
	private double price;
	private int quantite;
	
	// Constructor By Lombok || Sinon
	public Produit() {
		super();
		// TODO Auto-generated constructor stub
	}
		
	public Produit(Long id, String designation, double price, int quantite) {
		super();
		this.id = id;
		this.designation = designation;
		this.price = price;
		this.quantite = quantite;
	}

	// Getter and setter
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	
	public int getQuantite() {
		return quantite;
	}
	public void setQuantite(int quantite) {
		this.quantite = quantite;
	}
	
	
	
}
