package com.rproject.springboot.rprojectservices;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;

import com.rproject.springboot.rprojectservices.entities.Commentaire;
import com.rproject.springboot.rprojectservices.entities.Utilisateur;
import com.rproject.springboot.rprojectservices.entities.Voiture;

@SpringBootApplication
public class RprojectServicesApplication implements CommandLineRunner {

	@Autowired
	private RepositoryRestConfiguration restConfiguration;
	
	public static void main(String[] args) {
		SpringApplication.run(RprojectServicesApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		restConfiguration.exposeIdsFor(Voiture.class);
		restConfiguration.exposeIdsFor(Utilisateur.class); 
	}

}
