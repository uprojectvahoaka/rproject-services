package com.rproject.springboot.rprojectservices.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.rproject.springboot.rprojectservices.dao.CommentaireRepository;
import com.rproject.springboot.rprojectservices.entities.Commentaire;
import com.rproject.springboot.rprojectservices.entities.Utilisateur;
import com.rproject.springboot.rprojectservices.entities.Voiture;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class CommentaireRestServices {

	@Autowired
	private CommentaireRepository commentaireRepository;
	
	@PersistenceContext
	public EntityManager em;
	
	@GetMapping(value="/createCommentaire/{message}/{userId}/{voitureId}")
	public void createCommentaire(@PathVariable(name="message") String message, @PathVariable(name="userId") Long userId, @PathVariable(name="voitureId") Long voitureId){ 		
		Utilisateur user = new Utilisateur(userId);	
		Voiture voi = new Voiture(voitureId);		
		Commentaire addCom = new Commentaire(message, user, voi);				
		commentaireRepository.save(addCom);
	}
	
	@GetMapping(value="/getCommentaireByVoitureId/{voitureId}")
	public List<Utilisateur> listUtilisateurByName(@PathVariable(name="voitureId") Long voitureId){ 		
		return em.createQuery( "SELECT c FROM Commentaire c WHERE c.voiture_id = :voitureId ")
	    .setParameter("voitureId", voitureId)
	    .getResultList();
	}
	
}
