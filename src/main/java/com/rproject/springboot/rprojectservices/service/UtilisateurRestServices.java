package com.rproject.springboot.rprojectservices.service;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.rproject.springboot.rprojectservices.dao.UtilisateurRepository;
import com.rproject.springboot.rprojectservices.entities.Utilisateur;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class UtilisateurRestServices {

	@Autowired
	private UtilisateurRepository utilisateurRepository;
	
	@PersistenceContext
	public EntityManager em;

	@GetMapping(value="/listUtilisateurs")
	public List<Utilisateur> listUtilisateur(){
		return utilisateurRepository.findAll();
	}
	
	
	@GetMapping(value="/authentication/{nom}/{password}")
	public List<Utilisateur> listUtilisateurByName(@PathVariable(name="nom") String nom, @PathVariable(name="password") String password){ 		
		return em.createQuery( "SELECT id, nom, password, connected FROM Utilisateur c WHERE c.nom = :custName AND c.password = :custPassword ")
	    .setParameter("custName", nom)
	    .setParameter("custPassword", password)
	    .getResultList();
	}
	// findByNomAndPassword(@Param("nom") String nom, @Param("password") String password )
	
	@GetMapping(value="/getUtilisateursById/{id}")
	public Utilisateur getUtilisateurById(@PathVariable(name="id") Long id){
 		if(!(utilisateurRepository.findById(id).get().getNom()==null)) {
 			String isConnected = utilisateurRepository.findById(id).get().getConnected();
 			if(isConnected.equals("1")) {
 				return utilisateurRepository.findById(id).get();
 			}else {
 				Utilisateur noUser = new Utilisateur();
 	 			return noUser;
 			}
 			
 		}else {
 			Utilisateur noUser = new Utilisateur();
 			return noUser;
 		}
	}
	
	
}
