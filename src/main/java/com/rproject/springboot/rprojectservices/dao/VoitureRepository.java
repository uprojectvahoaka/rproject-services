package com.rproject.springboot.rprojectservices.dao;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import com.rproject.springboot.rprojectservices.entities.Produit;
import com.rproject.springboot.rprojectservices.entities.Voiture;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RepositoryRestResource
@ComponentScan("com.rproject.springboot.rprojectservices")
public interface VoitureRepository  extends JpaRepository<Voiture, Long>  {

}
