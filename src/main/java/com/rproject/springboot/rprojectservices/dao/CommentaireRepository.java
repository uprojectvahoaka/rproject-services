package com.rproject.springboot.rprojectservices.dao;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import com.rproject.springboot.rprojectservices.entities.Commentaire;
import com.rproject.springboot.rprojectservices.entities.Produit;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RepositoryRestResource
@ComponentScan("com.rproject.springboot")
public interface CommentaireRepository extends JpaRepository<Commentaire, Long> {

}
