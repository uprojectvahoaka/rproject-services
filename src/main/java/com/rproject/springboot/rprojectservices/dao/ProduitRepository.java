package com.rproject.springboot.rprojectservices.dao;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import com.rproject.springboot.rprojectservices.entities.Produit;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RepositoryRestResource
@ComponentScan("com.rproject.springboot.rprojectservices")
public interface ProduitRepository extends JpaRepository<Produit, Long> {

	
	/**
	 * Recherche par mot clé 
	 * */
	@RestResource(path="/byDesignation")
	public List<Produit> findByDesignationContains(@Param("des") String des );
	
	
	
	/**	
	 * Recherche par mot clé Pagination Request
	 * */
	@RestResource(path="/byDesignationPage")
	public Page<Produit> findByDesignationContains(@Param("des") String des , Pageable pageable);
	
}
