package com.rproject.springboot.rprojectservices.dao;

import java.util.List;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import com.rproject.springboot.rprojectservices.entities.Produit;
import com.rproject.springboot.rprojectservices.entities.Utilisateur;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RepositoryRestResource
@ComponentScan("com.rproject.springboot")
public interface UtilisateurRepository extends JpaRepository<Utilisateur, Long> {
	
}
